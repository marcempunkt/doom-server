import fs from "node:fs";
import path from "node:path";
import http, { Server } from "node:http";
import express, { Application, Request, Response } from "express";
import colors from "colors";

import Config from "./lib/config";
import { dbCleanup } from "./db/db";
import logger from "./middlewares/logger";
import cors from "./middlewares/cors";
import startSocketServer from "./socket/server";
import userRouter from "./routes/user";
import messageRouter from "./routes/message";
import postRouter from "./routes/post";
import updateRouter from "./routes/update";

dbCleanup(); // TODO do clean up on close not on startup

const config: Config = new Config();
const app: Application = express();
app.locals.config = config;
/** Initiate Socket server */
const server: Server = http.createServer(app);
startSocketServer(server, config);

/* Middlewares */
app.use(express.json());
logger.app(app);
cors.app(app);

// FIXME file upload limited to a small file size
// app.use(express.urlencoded({extended:true, limit:'50mb'})); 
// app.use(bodyParser.json({limit:'50mb'})); 
// app.use(bodyParser.urlencoded({extended:true, limit:'50mb'})); 

/* Routes */
app.get("/", (_req: Request, res: Response) => {
    return res.send("Server is up and running!")
});
app.use("/user", userRouter);
app.use("/message", messageRouter);
app.use("/post", postRouter);
app.use("/update", updateRouter);

server.listen(config.server.port, () => console.log(colors.green(`Server is listening localhost:${config.server.port}`)));

export default app;
