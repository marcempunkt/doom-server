import { Server, Socket } from "socket.io";
import colors from "colors";
import { PoolConnection } from "mariadb";

import pool from "../db/db";
import Config from "../lib/config";
import { UpdateSocketidEvent } from "../ts/types/socket_types";
import type { Message as ClientMessage, SendMessage, Pending, Friend as ClientFriend, Post, PostComment } from "../ts/types/client_types";
import { type Message as ServerMessage,
         type Friendrequest,
         Status } from "../ts/types/db_types";
import type { OkPacket } from "../ts/types/mariadb_types";
import { updateSocketid, getSocketids, getAllFriendIds, getAllFriendsSocketids } from "./server.utils";
import getIdFromToken from "../utils/getIdFromToken";

// TODO FIXME find the real types for WebRTC
type RTCSessionDescription = any;
type RTCSessionDescriptionInit = any;
type RTCIceCandidate = any;

/* --- Socket EventHandler ---*/
const handleDisconnect = async (auth: string, socket: Socket, io: Server, config: Config) => {
  /* set socketid to "offline" */
  await updateSocketid(UpdateSocketidEvent.DISCONNECT, socket, auth, config)
    .catch((err: string) => console.error(colors.red(`socket/EventHandler.handleDisconnect() => ${err}`)));
  socket.disconnect();
  /* emit change_status socket event to all friends of the currently connected socket */
  const userId: number | null = getIdFromToken(auth);
  if (!userId) return console.error(colors.red(`socket/EventHandler.handleDisconnect() => Unvalid authentication token.`));

  /* only update your online status to offline for all of your friends if you arent connected with another device */
  const clientSocketids: Array<string> = await getSocketids(userId);
  if (!clientSocketids.length) return await updateStatusHandler(userId, Status.OFF, socket, io); 
};

/* -------------------------------------------------------------------------------- Messages ---*/
const sendMessageHandler = async (msg: ServerMessage, socket: Socket, io: Server) => {
  /**
   * 
   */
  const msgBelongsToSender: boolean = msg.from_user === msg.owner;
  if (msgBelongsToSender) {
    const clientSocketids: Array<string> = await getSocketids(msg.from_user);
    clientSocketids.map((socketid: string) => {
      if (socketid === socket.id) return;
      console.log(`socket/EventHandler.sendMessageHandler() => emitted 'send_msg' event to ${socketid}`);
      io.to(socketid).emit("send_msg", msg);
    })
  }

  const msgShouldBeSendToRecipient: boolean = msg.to_user === msg.owner;
  if (msgShouldBeSendToRecipient) {
    const receiverSocketids: Array<string> = await getSocketids(msg.to_user);
    receiverSocketids.map((socketid: string) => {
      console.log(`socket/EventHandler.sendMessageHandler() => emitted 'send_msg' event to ${socketid}`);
      io.to(socketid).emit("send_msg", msg);
    });
  }
};

const deleteMessageHandler = async (deletedMessages: Array<ServerMessage> , socket: Socket, io: Server) => {
  /**
   * 1. for each msgId get the user it was sent to
   * 2. emit to this user all the msgIds so the client can delete them
   */
  deletedMessages.map(async (deletedMessage: ServerMessage) => {
    /* get the user to send the socket event to */
    const socketids: Array<string> = await getSocketids(deletedMessage.to_user);
    let clientSocketids: Array<string> = await getSocketids(deletedMessage.from_user);
    clientSocketids = clientSocketids.filter((socketid: string) => socketid !== socket.id);
    socketids.forEach((socketid: string) => io.to(socketid).emit('delete_msg', deletedMessages));
    clientSocketids.forEach((socketid: string) => io.to(socketid).emit('delete_msg', deletedMessages));
  });
};

const deleteChatHandler = async (userId: number, deleteChatUserId: number, socket: Socket, io: Server) => {
  let clientSocketids: Array<string> = await getSocketids(userId);
  clientSocketids = clientSocketids.filter((socketid: string) => socketid !== socket.id);
  return clientSocketids.forEach((socketid: string) => io.to(socketid).emit("delete_chat", deleteChatUserId));
};

/* -------------------------------------------------------------------------------- Friends ---*/
const sendFriendrequestHandler = async (fromUserid: number, toUserid: number, socket: Socket, io: Server) => {

  let conn: PoolConnection | undefined;

  try {
    conn = await pool.getConnection();
    if (!conn) throw console.error(colors.red(`socket/EventHandler.sendFriendrequestHandler => conn is undefined!`));

    const queryGetFriendrequest: string = `SELECT * FROM friendrequests WHERE from_user=? AND to_user=?`;
    const queryGetFriendrequestResult: Array<Friendrequest> = await conn.query(queryGetFriendrequest, [fromUserid, toUserid]);
    const friendrequest: Friendrequest | undefined = queryGetFriendrequestResult[0];

    if (!friendrequest) {
      return console.error(colors.red(
	`/socket/Eventhandler.sendFriendrequestHandler() => no friendrequest found with toUser:${toUserid} & fromUser:${fromUserid}`
      ));
    }

    const clientFriendrequest: Pending = {
      pendingId: friendrequest.id,
      fromUser: friendrequest.from_user,
      toUser: friendrequest.to_user,
    };

    const socketids: Array<string> = await getSocketids(toUserid);
    let clientSocketids: Array<string> = await getSocketids(fromUserid)
    clientSocketids = clientSocketids.filter((socketid: string) => socketid !== socket.id);

    socketids.forEach((socketid: string) => io.to(socketid).emit("send_friendrequest", clientFriendrequest));
    clientSocketids.forEach((socketid: string) => io.to(socketid).emit("send_friendrequest", clientFriendrequest));

  } catch (err: unknown) {
    throw console.error(colors.red(`socket/Eventhandler.sendSendFriendrequestHandler() => ${err}`));
  } finally {
    if (conn) return conn.release();
  }
};

const declineFriendrequestHandler = async (pending: Pending, socket: Socket, io: Server) => {
  /**
   * Remove friendrequest item from db
   */
  /* check to who we need to emit the socket event */
  const fromUserSocketids: Array<string> = await getSocketids(pending.fromUser);
  const toUserSocketids: Array<string> = await getSocketids(pending.toUser);
  const clientSocketid: string = socket.id;

  /* emit to all "decline_friendrequest" event but not to clientSocketid */
  fromUserSocketids.forEach((socketid: string) => {
    if (socketid !== clientSocketid) return io.to(socketid).emit("decline_friendrequest", pending.pendingId);
  });
  toUserSocketids.forEach((socketid: string) => {
    if (socketid !== clientSocketid) return io.to(socketid).emit("decline_friendrequest", pending.pendingId);
  });
};

const acceptFriendrequestHandler = async (pendingId: number, fromUser: number, toUser: number, socket: Socket, io: Server) => {
  /* check to who we need to emit the socket event */
  const friendSocketids: Array<string> = await getSocketids(toUser);
  let clientSocketids: Array<string> = await getSocketids(fromUser);
  clientSocketids.filter((socketid: string) => socketid !== socket.id);

  if (!friendSocketids.length) return console.log(`socket/EventHandler.acceptFriendrequestHandler() => no one to emit to.`);

  let conn: PoolConnection | undefined;

  try {
    conn = await pool.getConnection();
    if (!conn) throw console.error(colors.red(`socket/EventHandler.acceptFriendrequestHandler() => conn is undefined!`));

    /* Get the friend(-item) for the other user */
    const queryGetFriendItem: string = `SELECT 
                                       f.friend_with AS friendId,
                                       u.username AS friendName,
                                       u.status AS status,
                                       u.register_date AS registerDate,
                                       f.blocked AS blocked 
                                     FROM 
                                       users AS u
                                     INNER JOIN 
                                       friends AS f
                                     ON 
                                       u.id=f.friend_with
                                     WHERE 
                                       (f.user_id = ?)
                                     AND 
                                       (f.friend_with = ?)`;

    const queryGetFriendResult = await conn.query(queryGetFriendItem, [toUser, fromUser]);
    const queryGetClientFriendResult = await conn.query(queryGetFriendItem, [fromUser, toUser]);
    // this is the friend(-item) of the client who accepted the request
    const newFriendToFriends: ClientFriend | undefined = queryGetFriendResult[0]; 
    // this the new friend(-item) that will be send to the other client socketids
    const newFriendToClients: ClientFriend | undefined = queryGetClientFriendResult[0]; 
    if (!newFriendToFriends || !newFriendToClients) return console.error(colors.red(
      `socket/Eventhandler.acceptFriendrequestHandler() => no friend found in db`
    ));

    /* send the new friend to the other client you are logged in */
    clientSocketids.forEach((socketid: string) => io.to(socketid).emit("accept_friendrequest", pendingId, newFriendToClients));
    /* send yourself as a friend(-item) to all of this friends socketids */
    return friendSocketids.forEach((socketid: string) => io.to(socketid).emit("accept_friendrequest", pendingId, newFriendToFriends));

  } catch (err: unknown) {
    throw console.error(colors.red(`/api/socket/Eventhandler.ts/acceptFriendrequestHandler() => ${err}`));
  } finally {
    if (conn) return conn.release();
  }
};

const removeFriendHandler = async (clientId: number, friendId: number, socket: Socket, io: Server) => {
  const friendSocketids: Array<string> = await getSocketids(friendId);
  let clientSocketids: Array<string> = await getSocketids(clientId)
  clientSocketids = clientSocketids.filter((socketid: string) => socketid !== socket.id);

  clientSocketids.forEach((socketid: string) => io.to(socketid).emit("remove_friend", friendId));
  return friendSocketids.forEach((socketid: string) => io.to(socketid).emit("remove_friend", clientId));
};

/* -------------------------------------------------------------------------------- User ---*/
const updateAvatarHandler = async (userId: number, socket: Socket, io: Server) => {
  const allFriendsSocketids: Array<string> = await getAllFriendsSocketids(userId);
  let clientSocketids: Array<string> = await getSocketids(userId)
  clientSocketids = clientSocketids.filter((socketid: string) => socketid !== socket.id);

  clientSocketids.forEach((socketid: string) => io.to(socketid).emit("client_update_avatar"));
  return allFriendsSocketids.forEach((socketid: string) => io.to(socketid).emit("update_avatar", userId));
};

const updateStatusHandler = async (userId: number, status: Status, socket: Socket, io: Server) => {
  const allFriendsSocketids: Array<string> = await getAllFriendsSocketids(userId);
  let clientSocketids: Array<string> = await getSocketids(userId)
  clientSocketids = clientSocketids.filter((socketid: string) => socketid !== socket.id);

  clientSocketids.forEach((socketid: string) => io.to(socketid).emit("client_update_status", status));
  return allFriendsSocketids.forEach((socketid: string) => io.to(socketid).emit("update_status", userId, status));
};

const updateUsernameHandler = async (userId: number, username: string, socket: Socket, io: Server) => {
  const allFriendsSocketids: Array<string> = await getAllFriendsSocketids(userId);
  let clientSocketids: Array<string> = await getSocketids(userId)
  clientSocketids = clientSocketids.filter((socketid: string) => socketid !== socket.id);

  clientSocketids.forEach((socketid: string) => io.to(socketid).emit("client_update_username", username));
  return allFriendsSocketids.forEach((socketid: string) => io.to(socketid).emit("update_username", userId, username));
};

/* -------------------------------------------------------------------------------- Posts ---*/
const sendPostHandler = async (userId: number, newPost: Post, socket: Socket, io: Server) => {
  const friendSocketids: Array<string> = await getAllFriendsSocketids(userId);
  let clientSocketids: Array<string> = await getSocketids(userId)
  clientSocketids = clientSocketids.filter((socketid: string) => socketid !== socket.id);

  clientSocketids.forEach((socketid: string) => io.to(socketid).emit("send_post", newPost));
  return friendSocketids.forEach((socketid: string) => io.to(socketid).emit("send_post", newPost));
};

const deletePostHandler = async (userId: number, postId: number, socket: Socket, io: Server) => {
  const friendSocketids: Array<string> = await getAllFriendsSocketids(userId);
  let clientSocketids: Array<string> = await getSocketids(userId)
  clientSocketids = clientSocketids.filter((socketid: string) => socketid !== socket.id);

  clientSocketids.forEach((socketid: string) => io.to(socketid).emit("delete_post", postId));
  return friendSocketids.forEach((socketid: string) => io.to(socketid).emit("delete_post", postId));
};

const likeOrDislikePostHandler = async (userId: number, postId: number, socket: Socket, io: Server) => {
  const friendSocketids: Array<string> = await getAllFriendsSocketids(userId);
  let clientSocketids: Array<string> = await getSocketids(userId)
  clientSocketids = clientSocketids.filter((socketid: string) => socketid !== socket.id);

  clientSocketids.forEach((socketid: string) => io.to(socketid).emit("like_or_dislike_post", userId, postId));
  return friendSocketids.forEach((socketid: string) => io.to(socketid).emit("like_or_dislike_post", userId, postId));
};

const commentPostHandler = async (userId: number, postId: number, newComment: PostComment, socket: Socket, io: Server) => {
  const friendSocketids: Array<string> = await getAllFriendsSocketids(userId);
  let clientSocketids: Array<string> = await getSocketids(userId)
  clientSocketids = clientSocketids.filter((socketid: string) => socketid !== socket.id);

  clientSocketids.forEach((socketid: string) => io.to(socketid).emit("comment_post", postId, newComment));
  return friendSocketids.forEach((socketid: string) => io.to(socketid).emit("comment_post", postId, newComment));
};

const removeCommentPostHandler = async (userId: number, postId: number, commentId: number, socket: Socket, io: Server) => {
  const friendSocketids: Array<string> = await getAllFriendsSocketids(userId);
  let clientSocketids: Array<string> = await getSocketids(userId)
  clientSocketids = clientSocketids.filter((socketid: string) => socketid !== socket.id);

  clientSocketids.forEach((socketid: string) => io.to(socketid).emit("remove_comment_post", postId, commentId));
  return friendSocketids.forEach((socketid: string) => io.to(socketid).emit("remove_comment_post", postId, commentId));
};

/* -------------------------------------------------------------------------------- WebRTC ---*/
const startCallingHandler = async (data: { fromUser: number; toUser: number; key: string }, socket: Socket, io: Server) => {
  /**
   * "start_calling" event handler fn
   * add fromSocketid to the data and emit it to all the socketids of toUser
   * 
   * @params  {number}  data.fromUser: id of the user that emitted the "start_calling" event
   * @params  {number}  data.toUser:   id of the user to whom this event should be emitted
   * @params  {number}  data.key:      send the newly generated key to the other user to make Signaling more secure
   */
  const toUserSocketids: Array<string> = await getSocketids(data.toUser);
  const startCallingData = {
    fromUser: data.fromUser,
    toUser: data.toUser,
    fromSocketid: socket.id, /* extra added */
    key: data.key,
  };
  return toUserSocketids.forEach((socketid: string) => io.to(socketid).emit("start_calling", startCallingData));
};

const endCallingHandler = async (data: { fromUser: number; toUser: number; toSocketid?: string }, socket: Socket, io: Server) => {
  /**
   * "end_calling" fn handler fn
   * emit "end_calling" to all socketids of toUser unless there is toSocketid in the data object
   * then emit the event only to the remotes socket id
   * 
   * @params  {number}  data.fromUser:   id of the user that emitted the "end_calling" event
   * @params  {number}  data.toUser:     id of the user to whom this event should be emitted
   * @params  {string}  data.toSocketid: the socketid of the remote device
   */
  if (data.toSocketid) {
    /* send end_calling to a specific remote device */
    return io.to(data.toSocketid).emit("end_calling", data);
  } else {
    /* send end_calling to every device of toUser's id */
    const toUserSocketids = await getSocketids(data.toUser);
    return toUserSocketids.forEach((socketid: string) => io.to(socketid).emit("end_calling", data));
  }
};

const clientEndCallingHandler = async (data: { userId: number; calling: number }, socket: Socket, io: Server) => {
  /**
   * one client accepted the calling process
   * all other client that this user is logged in should not display callmodal anymore
   * @public
   * 
   * @params  {number}  data.userId      id of the user that emitted the "client_end_calling" event
   * @params  {number}  data.calling     id of the user whith whom he/she accepted the call 
   */
  let clientSocketids: Array<string> = await getSocketids(data.userId);
  clientSocketids = clientSocketids.filter((socketid: string) => socketid !== socket.id);
  return clientSocketids.map((socketid: string) => io.to(socketid).emit("client_end_calling", data));
};

const acceptCallingHandler = async (data: { fromUser: number; toUser: number; toSocketid: string }, socket: Socket, io: Server) => {
  /**
   * "accept_calling" event handler fn
   * add fromSocketid to the data object because now the Signaling Process will start
   * and that should only be betweeen two devices only
   * 
   * @params  {number}  data.fromUser:   id of the user that emitted the "end_calling" event
   * @params  {number}  data.toUser:     id of the user to whom this event should be emitted
   * @params  {string}  data.toSocketid: the socketid of the remote device
   */
  const acceptCallingData = {
    fromUser: data.fromUser,
    toUser: data.toUser,
    fromSocketid: socket.id, /* extra added */
    toSocketid: data.toSocketid,
  };
  return io.to(data.toSocketid).emit("accept_calling", acceptCallingData);
};

const signalingIceCandidateHandler = async (
  data: { fromUser: number; toUser: number, toSocketid: string; candidate: RTCIceCandidate | null; key: string },
  socket: Socket,
  io: Server
) => {
  /**
   * "signaling_icecandidate" event handler fn
   * emit ice candidate to the remoteSocketid (data.toSocketid)
   * 
   * @params  {number}  data.fromUser:   id of the user that emitted the "end_calling" event
   * @params  {number}  data.toUser:     id of the user to whom this event should be emitted
   * @params  {string}  data.toSocketid: the socketid of the remote device
   * @params  {RTCIceCandidate | null}   candidate
   * @params  {string}  key:             unless the key the Signaling process will fail
   */
  return io.to(data.toSocketid).emit("signaling_icecandidate", data);
};

const signalingOfferHandler = async (
  data: { fromUser: number; toUser: number; toSocketid: string; offer: RTCSessionDescriptionInit; key: string },
  socket: Socket,
  io: Server
) => {
  /**
   * "signaling_offer" event handler fn
   * emit the offer to the remote device
   * fromSocketid will be added for security reasons
   * 
   * @params  {number}  data.fromUser:   id of the user that emitted the "end_calling" event
   * @params  {number}  data.toUser:     id of the user to whom this event should be emitted
   * @params  {string}  data.toSocketid: the socketid of the remote device
   * @params  {RTCSessionDescriptionInit} offer
   * @params  {string}  key:             unless the key the Signaling process will fail
   */
  const signalingOfferData = {
    fromUser: data.fromUser,
    toUser: data.toUser,
    fromSocketid: socket.id, /* extra added */
    toSocketid: data.toSocketid,
    offer: data.offer,
    key: data.key,
  };
  return io.to(data.toSocketid).emit("signaling_offer", signalingOfferData);
};

const signalingAnswerHandler = async (
  data: { fromUser: number; toUser: number; fromSocketid: string; toSocketid: string; answer: RTCSessionDescriptionInit; key: string },
  socket: Socket,
  io: Server
) => {
  /**
   * "signaling_answer" event handler fn
   * after receiving an offer the remote will emit an answer to the other remote
   * with that the Signaling Process is complete
   * fromSocketid will be added for security reasons
   * 
   * @params  {number}  data.fromUser:   id of the user that emitted the "end_calling" event
   * @params  {number}  data.toUser:     id of the user to whom this event should be emitted
   * @params  {string}  data.toSocketid: the socketid of the remote device
   * @params  {RTCSessionDescriptionInit} answer
   * @params  {string}  key:             unless the key the Signaling process will fail
   */
  return io.to(data.toSocketid).emit("signaling_answer", data);
};


const screenshareOfferHandler = (data: { toSocketid: string; offer: RTCSessionDescriptionInit; key: string }, socket: Socket, io: Server) => {
  /**
   * Proxy the screenshare_offer socket event to data.toSocketid
   * @public
   *
   * @param  {string}                     data.toSocketid  socketid to whom this event should be proxied/emitted
   * @param  {RTCSessionDescriptionInit}  data.offer       webrtc offer
   * @param  {string}                     data.key         key for security reasons
   */
  io.to(data.toSocketid).emit("screenshare_offer", data);
};

const screenshareAnswerHandler = (data: { toSocketid: string; answer: RTCSessionDescriptionInit; key: string }, socket: Socket, io: Server) => {
  /**
   * Proxy the screenshare_answer socket event to data.toSocketid
   * @public
   *
   * @param  {string}                     data.toSocketid  socketid to whom this event should be proxied/emitted
   * @param  {RTCSessionDescriptionInit}  data.answer      webrtc answer 
   * @param  {string}                     data.key         key for security reasons
   */
  io.to(data.toSocketid).emit("screenshare_answer", data);
};

const screenshareIcecandidateHandler = (data: { toSocketid: string; candidate: RTCIceCandidate | null; key: string }, socket: Socket, io: Server) => {
  /**
   * Proxy the screenshare_icecandidate socket event to data.toSocketid
   * @public
   *
   * @param  {string}                     data.toSocketid  socketid to whom this event should be proxied/emitted
   * @param  {RTCIceCandidate | null}     data.candidate   webrtc icecandidate 
   * @param  {string}                     data.key         key for security reasons
   */
  io.to(data.toSocketid).emit("screenshare_icecandidate", data);
};

const screenshareRenegotiationOfferHandler = (data: { toSocketid: string; offer: RTCSessionDescriptionInit; key: string }, socket: Socket, io: Server) => {
  /**
   * Proxy the screenshare_renegotiation_offer socket event to data.toSocketid
   * @public
   *
   * @param  {string}                     data.toSocketid  socketid to whom this event should be proxied/emitted
   * @param  {RTCSessionDescriptionInit}  data.offer       webrtc offer
   * @param  {string}                     data.key         key for security reasons
   */
  io.to(data.toSocketid).emit("screenshare_renegotiation_offer", data);
};

const screenshareRenegotiationAnswerHandler = (data: { toSocketid: string; answer: RTCSessionDescriptionInit; key: string }, socket: Socket, io: Server) => {
  /**
   * Proxy the screenshare_renegotiation_answer socket event to data.toSocketid
   * @public
   *
   * @param  {string}                     data.toSocketid  socketid to whom this event should be proxied/emitted
   * @param  {RTCSessionDescriptionInit}  data.answer      webrtc answer
   * @param  {string}                     data.key         key for security reasons
   */
  io.to(data.toSocketid).emit("screenshare_renegotiation_answer", data);
};

const screenshareCloseHandler = (data: { toSocketid: string; key: string }, socket: Socket, io: Server) => {
  /**
   * Proxy the screenshare_close socket event to data.toSocketid
   * @public
   *
   * @param  {string}  data.toSocketid  socketid to whom this event should be proxied/emitted
   * @param  {string}  data.key         key for security reasons
   */
  io.to(data.toSocketid).emit("screenshare_close", data);
};

const hangupHandler = async (data: { fromUser: number; toUser: number; toSocketid: string }, socket: Socket, io: Server ) => {
  /**
   * "hangup" event handler fn
   * the call has been ended by one of the ones currently in a call
   * 
   * @params  {number}  data.fromUser:   id of the user that emitted the "end_calling" event
   * @params  {number}  data.toUser:     id of the user to whom this event should be emitted
   * @params  {string}  data.toSocketid: the socketid of the remote device
   */
  return io.to(data.toSocketid).emit("hangup", data);
};

const renegotiationOfferHandler = (
  data: { fromUser: number; toUser: number; toSocketid: string; offer: RTCSessionDescriptionInit; key: string },
  socket: Socket,
  io: Server
) => {
  return io.to(data.toSocketid).emit("renegotiation_offer", data);
};

const renegotiationAnswerHandler = (
  data: { fromUser: number; toUser: number; toSocketid: string; answer: RTCSessionDescriptionInit; key: string },
  socket: Socket,
  io: Server
) => {
  return io.to(data.toSocketid).emit("renegotiation_answer", data);
};

const callingReconnectedHandler = async (
  data: { key: string; fromUser: number; fromSocketid: string; inCallWith: number },
  socket: Socket,
  io: Server
) => {
  /**
   * A client is reconnected to the socket server and is now trying
   * get the remotes updated socketid and/or send its updated socketid
   * the other client.
   * @public
   * 
   * @params  {string}  key
   * @params  {number}  fromUser
   * @params  {number}  inCallWith
   * 
   * @returns   {void}  emit("calling_reconnected")
   */
  if (socket.id !== data.fromSocketid) return console.error(colors.red("socket/EventHandler.callingReconnectedHandler() => Socketid mismatch!"));

  const callingReconnectedData = {
    key: data.key,
    fromUser: data.fromUser,
    fromSocketid: data.fromSocketid,
    inCallWith: data.inCallWith,
  };

  console.log(colors.yellow(`callingReconnectedData: ${JSON.stringify(callingReconnectedData)}`));

  const inCallWithSocketids = await getSocketids(data.inCallWith);
  inCallWithSocketids.map((socketid: string) => io.to(socketid).emit("calling_reconnected", callingReconnectedData));
};

const callingReconnectedAnswerHandler = (
  data: { key: string; fromUser: number; fromSocketid: string; inCallWith: number, inCallWithSocketid: string },
  socket: Socket,
  io: Server
) => {
  /**
   * A client responded to the "calling_reconnected" event & now it will send its
   * (maybe) updated socketid back to the client that reconnected
   * @public
   * 
   * @params  {string}  key
   * @params  {number}  fromUser
   * @params  {string}  fromSocketid
   * @params  {number}  inCallWith
   * 
   * @returns   {void}  emit("calling_reconnected_answer")
   */
  if (socket.id !== data.inCallWithSocketid) return console.error(colors.red("socket/EventHandler.callingReconnectedAnswerHandler() => Socketid mismatch!"));

  const callingReconnectedAnswerData = {
    key: data.key,
    fromUser: data.fromUser,
    fromSocketid: data.fromSocketid,
    inCallWith: data.inCallWith,
    inCallWithSocketid: data.inCallWithSocketid,
  };

  console.log(colors.yellow(`callingReconnectedAnswerData: ${JSON.stringify(callingReconnectedAnswerData)}`));

  return io.to(data.fromSocketid).emit("calling_reconnected_answer", callingReconnectedAnswerData);
};

const SocketEventHandler = {
  handleDisconnect,
  sendMessageHandler,
  deleteMessageHandler,
  deleteChatHandler,
  sendFriendrequestHandler,
  declineFriendrequestHandler,
  acceptFriendrequestHandler,
  removeFriendHandler,
  updateAvatarHandler,
  updateStatusHandler,
  updateUsernameHandler,
  sendPostHandler,
  deletePostHandler,
  likeOrDislikePostHandler,
  commentPostHandler,
  removeCommentPostHandler,
  startCallingHandler,
  endCallingHandler,
  clientEndCallingHandler,
  acceptCallingHandler,
  signalingIceCandidateHandler,
  signalingOfferHandler,
  signalingAnswerHandler,
  screenshareOfferHandler,
  screenshareAnswerHandler,
  screenshareIcecandidateHandler,
  screenshareRenegotiationOfferHandler,
  screenshareRenegotiationAnswerHandler,
  screenshareCloseHandler,
  hangupHandler,
  renegotiationOfferHandler,
  renegotiationAnswerHandler,
  callingReconnectedHandler,
  callingReconnectedAnswerHandler,
};

export default SocketEventHandler;
