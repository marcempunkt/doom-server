import { Server, Socket } from "socket.io";
import colors from "colors";
import { PoolConnection, SqlError } from "mariadb";
import jwt, { JwtPayload } from "jsonwebtoken";

import getIdFromToken from "../utils/getIdFromToken";

import pool from "../db/db";
import Config from "../lib/config";
import type { OkPacket } from "../ts/types/mariadb_types";
import { UpdateSocketidEvent } from "../ts/types/socket_types";
import { Status } from "../ts/types/db_types";

export const updateSocketid = async (event: UpdateSocketidEvent, socket: Socket, token: string, config: Config) => {
  /**
   * update the socketid of a user
   * @public
   * 
   * @returns  {Promise<string>}  String of what happended
   */
  let query: string;

  /* check if the token is valid */
  try {
    if (!config.auth.secret) throw new Error("Secret token is undefined.");
    const jwtPayload: string | JwtPayload = jwt.verify(token, config.auth.secret);
    if (typeof jwtPayload === "string") throw new Error(`Unvalid authentication token`);
  } catch (err: unknown) {
    throw new Error(`${err}`);
  }

  const userId: number | null = getIdFromToken(token);
  if (!userId) throw new Error(`Unvalid authentication token.`);


  if (event === UpdateSocketidEvent.CONNECT) {
    /* create a new entry in socketids */
    query = `INSERT INTO socketids (socketid, user_id) VALUES (?, ?)`;
  } else if (event === UpdateSocketidEvent.DISCONNECT) {
    /* remove the already existing entry in socketids with the socket.id */
    query = `DELETE FROM socketids WHERE socketid=? AND user_id=?`;
  } else {
    throw new Error(`Unknown event in updating socketid: ${event}`);
  }

  /* Update socketid to users db */
  let conn: PoolConnection | undefined;

  try {
    conn = await pool.getConnection();
    if (!conn) throw console.error(colors.red(`socket/server.utils.updateSocketid() => conn is undefined!`));

    // const result: OkPacket = await conn.query(query, [socket.id, userId]);
    const result: OkPacket = await conn.query(query, [socket.id, userId])
				       .catch((err: SqlError) => console.error(colors.red(`socket/server.utils.updateSocketid() => ${err}`)));

    if (event === UpdateSocketidEvent.CONNECT) {
      console.log(`socket/EventHandler.updateSocketid() => New Socket connected with ID: ${socket.id}`);
    } else if (event === UpdateSocketidEvent.DISCONNECT) {
      console.log(`socket/EventHandler.updateSocketid() => Socket disconnected with ID: ${socket.id}`);
    }

    return `Updated SocketId: ${JSON.stringify(result)}`;

  } catch (err: unknown) {
    throw new Error(`${err}`);
  } finally {
    if (conn) return conn.release();
  }
};

export const getAllFriendsSocketids: (userId: number) => Promise<Array<string>> = async (userId) => {
  /**
   * gets the socketid of a user by its id
   * @public
   * 
   * @param    {number}             userId    should only be unsigned integer
   * 
   * @returns  {Promise<string[]>}  socketid  if the socketid is an empty string, it menas that it failed to find a user in the db!
   */
  /* userId should be unsigned integer & not be float */
  const allFriendIds: Array<number> = await getAllFriendIds(userId);

  let allFriendSocketids: Array<string> = [];

  const eachFriendsSocketids: Array<string[]> = await Promise.all(allFriendIds.map(async (friendId: number) => {
    const socketids: Array<string> = await getSocketids(friendId);
    return socketids;
  }));

  eachFriendsSocketids.forEach((socketids: Array<string>) => {
    socketids.forEach((socketid: string) => allFriendSocketids.push(socketid));
  });

  return allFriendSocketids;
};

export const getSocketids: (userId: number) => Promise<Array<string>> = async (userId) => {
  /**
   * gets the socketid of a user by its id
   * @public
   * 
   * @param    {number}             userId    should only be unsigned integer
   * 
   * @returns  {Promise<string[]>}  socketid  if the socketid is an empty string, it menas that it failed to find a user in the db!
   */
  /* userId should be unsigned integer & not be float */
  if (userId < 0 || !Number.isInteger(userId)) {
    console.error(colors.red(`socket/server.utils.getSocketids() => param userId is float or minus: ${userId}`));
    return [];
  }

  let conn: PoolConnection | undefined;

  try {
    conn = await pool.getConnection();
    if (!conn) throw console.error(colors.red(`socket/server.utils.getSocketids() => conn is undefined!`));

    const querySocketid: string = `SELECT socketid FROM socketids WHERE user_id=?`;
    const querySocketidResult: Array<{ socketid: string }> = await conn.query(querySocketid, [userId]);
    const socketids: Array<string> = querySocketidResult.map((obj: { socketid: string }) => obj.socketid);

    return socketids;

  } catch (err: unknown) {
    console.error(colors.red(`socket/server.utils.getSocketids() => ${err}`));
    return [];
  } finally {
    if (conn) { conn.release(); }
  }
};

export const getAllFriendIds: (userId: number) => Promise<Array<number>> = (userId) => {
  return new Promise(async (resolve, reject) => {
    /* userId should be unsigned integer & not be float */
    if (userId < 0 || !Number.isInteger(userId)) {
      console.error(colors.red(`socket/server.utils.getAllFriendIds() => Bad user id: ${userId}`));
      return resolve([]);
    }

    let conn: PoolConnection | undefined;

    try {
      conn = await pool.getConnection();
      if (!conn) throw console.error(colors.red(`socket/server.utils.getAllFriendIds() => conn is undefined!`));

      const queryGetAllFriendIds: string = `SELECT friend_with FROM friends WHERE user_id=?`;
      const getAllFriendIdsResult = await conn.query(queryGetAllFriendIds, [userId]);
      const allFriendIds: Array<number> = getAllFriendIdsResult.map((obj: { friend_with: number }) => obj.friend_with);

      return resolve(allFriendIds);
    } catch(err: unknown) {
      console.error(colors.red(`${err}`));
      return resolve([]);
    } finally {
      if (conn) return conn.release();
    }
  });
};

const SocketServerUtils = {
  updateSocketid,
  getSocketids,
  getAllFriendIds,
};

export default SocketServerUtils;
