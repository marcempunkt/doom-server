import { Express, Request } from "express";
import multer, { Multer, FileFilterCallback } from "multer";
import colors from "colors";
import sharp from "sharp";
import fs from "fs";
import path from "path";
import { PoolConnection } from "mariadb";

import { OkPacket } from "../ts/types/mariadb_types";
import pool from "../db/db";

const fileFilter = (req: Request, file: Express.Multer.File, cb: FileFilterCallback) => {
  enum AcceptableFileType {
    PNG = "image/png",
    JPG = "image/jpeg",
    GIF = "image/gif",
  };

  if (file.mimetype === AcceptableFileType.PNG ||
      file.mimetype === AcceptableFileType.JPG ||
      file.mimetype === AcceptableFileType.GIF) {
    file.filename= "1";
    return cb(null, true);
  } else {
    console.error(colors.red("PostController.postImages.fileFilter => wrong file type: ${file.mimetype}"));
    return cb(null, false);
  }
};

/* Multer does not work with typescript
 * TODO expand the type declaration of upload & Multer to make the code below work */
// @ts-ignore
const upload: Multer = multer({
  // storage: storage,
  dest: "files/post/",
  limits: { files: 5, fileSize: 10000000 },
  fileFilter: fileFilter
}).array("images", 5);

const renamePostPictures: (files: Array<Express.Multer.File>, postId: number) => Promise<Array<string>> = async (files, postId) => {
  const newFilenames: Array<string> = await Promise.all(files.map(async (file: Express.Multer.File) => {
    const newFilename: string = `${"post-" + postId + "-" + new Date().valueOf() + "-" + Math.round(Math.random() * 1E20)}.gif`;
    await sharp(file.path)
      // .resize(200, 200)
      .flatten({ background: "#000" })
      .gif()
      .toFile(path.join(file.destination, newFilename))
      .catch((err: unknown) => console.error(colors.red(`PostControllerUtils.renamePostPictures => ${err}`)));
    return newFilename;
  }));
  return newFilenames;
};

const deletePostPictures = async (files: Array<Express.Multer.File>) => {
  files.forEach(async (file: Express.Multer.File) => {
    await fs.promises.rm(file.path)
	    .catch((err: unknown) => console.error(colors.red(`PostControllerUtils.deletePostPictures => ${err}`)));
  });
};

const deletePostPicturesFromFiles = async () => {
  return;
};

const handlePostPostImages: (images: Array<Express.Multer.File>, postId: number) => Promise<Array<string>> = async (images, postId) => {

  /* Create an array out of Express.Multer.File(List) */
  // const uploadedFiles: Array<Express.Multer.File> = Object.assign(images);

  /* Pic has been uploaded
   * now check if the token is valid
   * check if the id from the token is still a user inside the db
   * rename the file to the id of the user */
  const postPictureFilenames: Array<string> = await renamePostPictures(images, postId);
  await deletePostPictures(images);

  let conn: PoolConnection | undefined;

  try {
    conn = await pool.getConnection();
    if (!conn) throw console.error(colors.red("PostControllerUtils.handlePostPostImages() => conn is undefined!"));

    postPictureFilenames.forEach(async (filename: string) => {
      if (!conn) throw console.error(colors.red("PostControllerUtils.handlePostPostImages() => conn is undefined!"));
      /* Save images filename to db */
      const queryPostImage: string = `INSERT INTO post_images(post_id, filename) VALUES(?, ?)`;
      const queryPostImageResult: OkPacket = await conn.query(queryPostImage, [postId, filename]);
      // console.log(`PostControllerUtils.handlePostPostImages() => added images to post ${JSON.stringify(queryPostImageResult)}`);
    });

  } catch(err: unknown) {
    console.error(colors.red(`PostControllerUtils.handlePostPostImages() => ${err}`));
  } finally {
    if (conn) { conn.release(); }
    return postPictureFilenames;
  }

};

const PostControllerUtils = {
  upload,
  renamePostPictures,
  deletePostPictures,
  deletePostPicturesFromFiles,
  handlePostPostImages,
};

export default PostControllerUtils;
