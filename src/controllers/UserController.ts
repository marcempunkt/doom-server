import { Request, Response } from "express";
import colors from "colors";
import bcrypt from "bcrypt";
import jwt, { JwtPayload, Secret } from "jsonwebtoken";
import { ValidationError } from "@hapi/joi";
import { SqlError, PoolConnection } from "mariadb";

import pool from "../db/db";
import type { OkPacket } from "../ts/types/mariadb_types";
import type { ResponseBody, AuthLocals } from "../ts/types/express_types";
import { User } from "../ts/types/db_types";
import getIdFromToken from "../utils/getIdFromToken";
import UserControllerUtils from "./UserController.Utils";

// FIXME TODO This route kinda seems unnecessary but its used in Babbeln.tsx so double check if this is neccessary anymore
const validateToken = async (
    req: Request<{}, {}, { token: string }>,
    res: Response<boolean>
) => {
    /**
     * Validate the authentication token
     * check if the user is authorized
     *
     * @body     {string}   auth 
     * 
     * @returns  {boolean}  isAuthorized
     */
    if (!req.body.token) return res.status(401).send(false);
    // @ts-ignore typing FIXME typing
    const secret: string = req.app.locals.config.auth.secret;

    let conn: PoolConnection | undefined;

    try {
        conn = await pool.getConnection();
        if (!conn) throw console.error(colors.red(`UserController.validateToken() => conn is undefined!`));

        try {
            const jwtPayload: string | JwtPayload = jwt.verify(req.body.token, secret);

            /* unvalid jwt */
            if (typeof jwtPayload === "string") return res.status(401).send(false);

            const loggedinUserId: number = jwtPayload.id;

            const query: string = `SELECT id FROM users WHERE id=?`;
            const queryResult = await conn.query(query, [loggedinUserId]);

            const loggedinUser: { id: number } | undefined = queryResult[0];
            if (!loggedinUser) return res.status(401).send(false);

            return res.status(200).send(true);

        } catch (err: unknown) {
            return res.status(401).send(false);
        }

    } catch (err: unknown) {
        res.status(500).send(false);
        throw console.error(colors.red(`UserController.validateToken() => ${err}`));
    } finally {
        if (conn) return conn.release();
    }
};

const registerUser = async (
    req: Request<{}, {}, { username: string; email: string; password: string }>,
    res: Response<ResponseBody>
) => {
    /**
     * Register a new user
     *
     * @body  {string}  username     
     * @body  {string}  email     
     * @body  {string}  password     
     * 
     * @returns {Success|Error}
     */

    /* Register Validation */
    let validationError: ValidationError | undefined = UserControllerUtils.registerValidation(req.body).error;
    /* Wrong validation */
    if (validationError) return res.status(400).send({ message: `${validationError.details[0].message}` });

    let conn: PoolConnection | undefined;

    try {
        conn = await pool.getConnection();
        if (!conn) throw console.error(colors.red(`UserController.registerUser() => conn is undefined!`));

        /* Check if User is already in our DB */
        const queryCheckEmail: string = `SELECT * FROM users WHERE email = ?`;
        const queryCheckEmailResult = await conn.query(queryCheckEmail, [req.body.email]);
        const emailExist: User | undefined = queryCheckEmailResult[0];

        const queryCheckUsername: string = `SELECT * FROM users WHERE username = ?`;
        const queryCheckUsernameResult = await conn.query(queryCheckUsername, [req.body.username]);
        const usernameExist: User | undefined = queryCheckUsernameResult[0];

        /* Email already exists */
        if (emailExist) { return res.status(400).send({ message: "Email already exists." }) };
        /* Username already exists */
        if (usernameExist) { return res.status(400).send({ message: "Username already exists." }) };
        /* bcrypt hash password & create new user */
        let salt: string = await bcrypt.genSalt(10);
        let hashedPassword: string = await bcrypt.hash(req.body.password, salt);
        /* create a new user */
        const queryCreateNewUser: string = `INSERT INTO 
                                        users(username, email, password) 
                                        values(?, ?, ?)`;

        /* I don't know how to really error handle INSERT TO thats why I'll use callback form */
        conn.query(queryCreateNewUser, [req.body.username, req.body.email, hashedPassword]);
        console.log(`UserController.registerUser() => a new user was created with the name ${req.body.username}`);
        return res.send({ message: "You successfully created a new Account." });

    } catch (err: unknown) {
        res.status(500).send({ message: `Couldn't create a new account.` });
        throw console.error(colors.red(`UserController.registerUser() => ${err}`));
    } finally {
        if (conn) conn.release();
    }
};

const loginUser = async (
    req: Request<{}, {}, { email: string; password: string }>,
    res: Response<ResponseBody & { auth?: string }>
) => {
    /**
     * Login
     * 
     * @body  {string}  email
     * @body  {string}  password
     * 
     * @returns { message: { string } }
     * @returns { auth?: { string } }
     */
    const errorMessage: string = "E-mail or Password is wrong!";
    // @ts-ignore FIXME typing
    const secret: string = req.app.locals.config.auth.secret;

    /* Validation of email & password */
    const validationError: ValidationError | undefined = UserControllerUtils.loginValidation(req.body).error;
    /* Wrong input */
    if (validationError) return res.status(400).send({ message: `${validationError.details[0].message}` });

    let conn: PoolConnection | undefined;

    try {
        conn = await pool.getConnection();
        if (!conn) throw console.error(colors.red(`UserController.loginUser() => conn is undefined!`));

        /* Check if an user with that email does exist */
        const queryCheckEmail = `SELECT * FROM users WHERE email = ?`;
        const queryCheckEmailResult = await conn.query(queryCheckEmail, [req.body.email]);
        const emailExist: User | undefined = queryCheckEmailResult[0];

        /* 400 : Email doesn't exists */
        if (!emailExist) return res.status(400).send({ message: errorMessage });

        /* Wrong password */
        const passwordIsCorrect: boolean = await bcrypt.compare(req.body.password, emailExist.password);
        if (!passwordIsCorrect) return res.status(400).send({ message: errorMessage });

        /* Create authentication token (jwt) */
        const token = jwt.sign({ id: emailExist.id }, secret);

        /* Successfully logged in!! */
        console.log(`UserController.loginUser() => user with the username ${emailExist.username} has logged in`);
        return res.status(200).send({
            message: "Successfully logged in.",
            auth: `${token}`
        });

    } catch (err: unknown) {
        res.status(500).send({ message: `Failed to login` });
        throw console.error(colors.red(`UserController.loginUser() => ${err}`));
    } finally {
        if (conn) conn.release();
    }
};

const deleteUser = async (
    req: Request<{}, {}, { password: string }>,
    res: Response<ResponseBody, AuthLocals>
) => {
    /**
     * Delete your account
     * 
     * @body  {string}  password
     * 
     * @returns  {Success|Error}
     */
    let conn: PoolConnection | undefined;

    try {
        conn = await pool.getConnection();
        if (!conn) throw console.error(colors.red(`UserController.deleteUser() => conn is undefined!`));
        /* Check password */
        const queryGetUsernameAndPassword: string = `SELECT password, username FROM users WHERE id=?`;
        const queryGetUsernameAndPasswordResult = await conn.query(queryGetUsernameAndPassword, [res.locals.userId]);
        const usernameAndPassword: { password: string; username: string; } | undefined = queryGetUsernameAndPasswordResult[0];
        if (!usernameAndPassword) return res.status(400).send({ message: `No user found with the id ${res.locals.userId}.` });

        const queryUsername: string = usernameAndPassword.username;
        const queryPassword: string = usernameAndPassword.password;
        const isValidPassword: boolean = await bcrypt.compare(req.body.password, queryPassword);
        if (!isValidPassword) return res.status(401).send({ message: "Wrong email or password!" });

        /* Delete Account for real */
        const queryDelete: string = `DELETE FROM users WHERE id=?`;
        conn.query(queryDelete, [res.locals.userId])
        console.log(`UserController.deleteUser() => deleted user with the id: ${res.locals.userId}`);
        return res.status(200).send({ message: `The are no traces of ${queryUsername} anymore!` });

    } catch(err: unknown) {
        res.status(500).send({ message: "Internal Server Error: We couldn't delete your account!" });
        throw console.log(colors.red(`UserController.deleteUser() => ${err}`));
    } finally {
        if (conn) return conn.release();
    }
};


const UserController = {
    validateToken,
    registerUser,
    loginUser,
    deleteUser,
};

export default UserController;
