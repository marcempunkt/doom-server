import { Request, Response } from "express";
import bcrypt from "bcrypt";
import { PoolConnection, SqlError } from "mariadb";
import colors from "colors";
import { ValidationError } from "@hapi/joi";
import multer from "multer";

import pool from "../db/db";
import UserProfileChangeControllerUtils from "./UserProfileChangeController.Utils";
import type { OkPacket } from "../ts/types/mariadb_types";
import { type User, Status } from "../ts/types/db_types";
import type { ResponseBody, AuthLocals } from "../ts/types/express_types";

const changeUsername = async (req: Request, res: Response) => {
    /**
     * TODO Change username
     * 
     * @body
     * 
     * @returns  {Success|Error}
     */
    return res.send({ message: "in progress" });
}

const changeEmail = async (
    req: Request<{}, {}, { oldEmail: string; newEmail: string; password: string }>,
    res: Response<ResponseBody, AuthLocals>
) => {
    /**
     * Change E-mail
     * 
     * @body  {string}  oldEmail
     * @body  {string}  newEmail
     * @body  {string}  password
     * 
     * @returns  {Success|Error}
     */

    /* Check if old email is the same as new email */
    const sameEmailError: string = "New email can't be same as the old one.";
    if (req.body.oldEmail === req.body.newEmail) return res.status(400).send({ message: sameEmailError });

    let conn: PoolConnection | undefined;

    try {
        conn = await pool.getConnection();
        if (!conn) throw console.error(colors.red(`ProfileChangeController.changeEmail() => conn is undefined!`));

        /* Search for the user in the DB */
        const queryGetUser: string = `SELECT * FROM users WHERE email=? AND id=?`;
        const getUserResult = await conn.query(queryGetUser, [req.body.oldEmail, res.locals.userId]);
        const user: User | undefined = getUserResult[0];

        if (!user) {
            console.error(colors.red(
                `ProfileChangeController.changeEmail() => Couldn't find user with the the email ${req.body.oldEmail} and id ${res.locals.userId}`
            ));
            return res.status(400).send({ message: "Couldn't find user with your email address ${body.oldEmail}"});
        }

        /* Compare passwords */
        const validPassword = await bcrypt.compare(req.body.password, user.password);
        if (!validPassword) return res.status(401).send({ message: "Password is wrong" });

        /* Check if email is the same as the current one in DB */
        if (req.body.newEmail === user.email) return res.status(400).send({ message: sameEmailError })

        /* check if new email is already used by another user */
        const queryCheckEmail = `SELECT * from users WHERE email=?`;
        const checkEmailResult = await conn.query(queryCheckEmail, [req.body.newEmail]);
        const userWithSameEmail: User | undefined = checkEmailResult[0];
        if (userWithSameEmail) return res.status(400).send({ message: "The new email is already being used by another user" });

        /* Change Email to new one */
        const queryChangeEmail: string = `UPDATE users SET email=? WHERE email=? AND id=?`
        // const changedEmail: OkPacket = await conn.query(queryChangeEmail, [body.newEmail, body.oldEmail, body.userId]);

        conn.query(queryChangeEmail, [req.body.newEmail, req.body.oldEmail, res.locals.userId]);
        return res.status(200).send({ message: `Email changed to ${req.body.newEmail}` });

    } catch(err: unknown) {
        res.status(500).send({ message: "Couldn't change email." });
        throw console.error(colors.red(`ProfileChangeController.changeEmail() => ${err}`));
    } finally {
        if (conn) conn.release();
    }
};

const changePassword = async (
    req: Request<{}, {}, { oldPassword: string; newPassword: string }>,
    res: Response<ResponseBody, AuthLocals>
) => {
    /**
     * Change Password
     * 
     * @body  {string}  oldPassword
     * @body  {string}  newPassword
     * 
     * @returns  {Success|Error}
     */

    /* validate new password */
    const validationError: ValidationError | undefined = UserProfileChangeControllerUtils
        .changePasswordValidation({ password: req.body.newPassword }).error;
    if (validationError) return res.status(400).send({
        message: `${validationError.details[0].message}`
    });

    let conn: PoolConnection | undefined;

    try {
        conn = await pool.getConnection();
        if (!conn) throw console.error(colors.red(`ProfileChangeController.changePassword() => conn is undefined!`));

        /* check if oldPassword matches the real password */
        const queryGetPassword: string = `SELECT password FROM users WHERE id=?`;
        const getPasswordResult = await conn.query(queryGetPassword, [res.locals.userId]);
        const realPassword: string | undefined = getPasswordResult[0].password;
        if (!realPassword) return res.status(500).send({ message: `Couldn't find any user with your id ${res.locals.userId}.` });

        const validPassword: boolean = await bcrypt.compare(req.body.oldPassword, realPassword);
        if (!validPassword) {
            /* 401: Unauthorized Access : Password is wrong */
            console.error(colors.red(`ProfileChangeController.changePassword() => wrong password`));
            return res.status(401).send({ message: "Unvalid Password!" });
        }

        /* change password */
        const salt = await bcrypt.genSalt(10);
        const hashedPassword: string = await bcrypt.hash(req.body.newPassword, salt);

        const queryChangePassword: string = `UPDATE users SET password=? WHERE id=?`;
        conn.query(queryChangePassword, [hashedPassword, res.locals.userId]);
        return res.status(200).send({ message: "Changed password to new one" });

    } catch(err: unknown) {
        res.status(500).send({ message: "Couldn't change password" });
        throw console.log(colors.red(`ProfileChangeController.changePassword() => ${err}`));
    } finally {
        if (conn) conn.release();
    }
};

const changeStatus = async (
    req: Request<{}, {}, { newStatus: Status }>,
    res: Response<ResponseBody, AuthLocals>
) => {
    /**
     * Change Status
     * 
     * @body  {Status}  newStatus 
     * 
     * @returns  {Success|Error}
     */

    /* check status if it is really of type status */
    let newStatus: Status;
    switch (req.body.newStatus) {
        case Status.ON:
            newStatus = Status.ON;
            break;
        case Status.OFF:
            newStatus = Status.OFF;
            break;
        case Status.DND:
            newStatus = Status.DND;
            break;
        case Status.AWAY:
            newStatus = Status.AWAY;
            break;
        default:
            newStatus = Status.ON;
            break;
    };
    
    let conn: PoolConnection | undefined;

    try {
        conn = await pool.getConnection();
        if (!conn) throw console.error(colors.red(`ProfileChangeController.changeStatus() => conn is undefined!`));

        const queryChangeStatus: string = `UPDATE users SET status=? WHERE id=?`;
        conn.query(queryChangeStatus, [newStatus, res.locals.userId]);
        return res.send({ message: `Status changed to ${newStatus}` });

    } catch(err: unknown) {
        res.status(500).send({ message: "Could not update your status." });
        throw console.error(colors.red(`ProfileChangeController.changeStatus() => ${err}`));
    } finally {
        if (conn) conn.release();
    }
};

// TODO typing for req.file
const changeAvatar = (
    req: Request,
    res: Response<ResponseBody, AuthLocals>
) => {
    /**
     * Change avatar
     * 
     * @param  {avatar}  Express.Multer.File
     * 
     * @returns  {Success|Error}
     */

    /* Multer does not work with typescript
     * TODO expand the type declaration of upload & Multer to make the code below work */
    // @ts-ignore
    UserProfileChangeControllerUtils.upload(req, res, async (err: Error) => {
        if (err instanceof multer.MulterError) {
            console.error(colors.red(`ProfileChangeController.changeAvatar() => ${err}`));
            return res.status(500).send({ message: `${err}` });
        } else if (err) {
            console.error(colors.red(`ProfileChangeController.changeAvatar() => ${err}`));
            return res.status(500).send({ message: `${err}` });
        } else if (!req.file) {
            console.error(colors.red(`ProfileChangeController.changeAvatar() => req.file is unknown`));
            return res.status(500).send({ message: `Try uploading a .png, .jpg or .gif image the next time.` });
        }
        /* Everything went fine. */
        await UserProfileChangeControllerUtils.renameAvatar(req.file, res.locals.userId);
        await UserProfileChangeControllerUtils.deleteAvatar(req.file);

        return res.send({ message: "Avatar has been successfully changed." });
    });
};

const UserProfileChangeController = {
    changeUsername,
    changeEmail,
    changePassword,
    changeStatus,
    changeAvatar,
};

export default UserProfileChangeController;
