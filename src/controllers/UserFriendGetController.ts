import { Request, Response } from "express";
import colors from "colors";
import { PoolConnection } from "mariadb";

import pool from "../db/db";
import type { Friend as ClientFriend, Pending } from "../ts/types/client_types";
import type { ResponseBody, AuthLocals } from "../ts/types/express_types";
import { Status } from "../ts/types/db_types";
import { getSocketids } from "../socket/server.utils";

const getAll = async (
    req: Request,
    res: Response<Array<ClientFriend> | ResponseBody, AuthLocals>
) => {
    /**
     * Get all friends of a user
     * this should replace blocked/friends route
     * 
     * @params  {string}  token
     * 
     * @returns  {Friend[]}
     */
    let conn: PoolConnection | undefined;

    try {
        conn = await pool.getConnection();
        if (!conn) throw console.error(colors.red(`UserFriendGetController.getAll() => conn is undefined!`));

        const query: string = `SELECT 
                                  f.friend_with AS friendId,
                                  u.username AS friendName,
                                  u.status AS status,
                                  u.register_date AS registerDate,
                                  f.blocked AS blocked 
                              FROM 
                                  users AS u
                              INNER JOIN 
                                  friends AS f
                              ON 
                                  u.id=f.friend_with
                              WHERE 
                                  (f.user_id = ?)
                              AND 
                                  (f.friend_with != ?)`;

        const queryResult = await conn.query(query, [res.locals.userId, res.locals.userId]);
        const friends: Array<ClientFriend> = [...queryResult];
        /* if status is anything but off (invisible) check if the friend_id has a socketid 
         * if he/she has a socketid => leave status as it is
         * if not set status to off
         * therefore the state of status can be preserved while showing correctly if a user is online or not */
        const updatedFriends: Array<ClientFriend> = await Promise.all(friends.map(async (friend: ClientFriend) => {
            const socketids: Array<string> = await getSocketids(friend.friendId);
            if (!socketids.length) {
	        friend.status = Status.OFF;
            }
            return friend;
        }));
        return res.send(updatedFriends);
        
    } catch (err: unknown) {
        res.status(500).send({ message: "Couldn't get your friends." });
        throw console.error(colors.red(`UserFriendGetController.getAll() => ${err}`));
    } finally {
        if (conn) conn.release();
    }
};

const getPendings = async (
    req: Request,
    res: Response<Array<Pending> | ResponseBody, AuthLocals>
) => {
    /**
     * Get all pending friendrequests of a User
     * 
     * @returns  {Pending[]}
     */
    let conn: PoolConnection | undefined;

    try {
        conn = await pool.getConnection();
        if (!conn) throw console.error(colors.red(`UserFriendGetController.getPendings() => conn is undefined!`));

        const query: string = `SELECT 
                                   fr.id as pendingId, 
                                   fr.from_user AS fromUser, 
                                   fr.to_user AS toUser
                               FROM 
                                   users AS u
                               INNER JOIN 
                                   friendrequests AS fr
                               ON 
                                   (u.id=fr.from_user OR u.id=fr.to_user)
                               WHERE 
                                   (u.id!=?)
                               AND 
                                   (fr.from_user=? OR fr.to_user=?)
                               AND 
                                   (fr.accepted=0)`;

        const queryResult = await conn.query(query, [res.locals.userId, res.locals.userId, res.locals.userId]);
        const pendingItems: Array<Pending> = [...queryResult];
        return res.send(pendingItems);
        
    } catch (err: unknown) {
        res.status(500).send({ message: "Couldn't get your pending friendrequests." });
        throw console.error(colors.red(`UserFriendGetController.getPendings() => ${err}`));
    } finally {
        if (conn) conn.release();
    }
};

const UserFriendGetController = {
    getAll,
    getPendings,
};

export default UserFriendGetController;
