import { Request, Response } from "express";
import jwt, { JwtPayload } from "jsonwebtoken";
import { SqlError, PoolConnection } from "mariadb";
import colors from "colors";

import pool from "../db/db";
import type { OkPacket } from "../ts/types/mariadb_types";
import type { Friend as ServerFriend, Friendrequest } from "../ts/types/db_types";
import type { Friend as ClientFriend } from "../ts/types/client_types";
import type { Pending } from "../ts/types/client_types";
import type { ResponseBody, AuthLocals } from "../ts/types/express_types";

const fuzzySearchUsers = async (
    req: Request<{ query: string }>,
    res: Response<Array<{ id: number; username: string }> | ResponseBody>
) => {
    /**
     * fuzzy search users
     * 
     * @param    {string}  query
     * 
     * @returns  {{ id: number; username: string }[]}  Users
     */

    /* using the ? placeholder won't work here because mariadb will insert it like that: '%"placeholder"%'
     * which won't match because of the extra " qoutes */
    const regexSqi: RegExp = /[\t\r\n]|(--[^\r\n]*)|(\/\*[\w\W]*?(?=\*)\*\/)/gi; 

    if (regexSqi.test(req.params.query)) {
        console.error(colors.red(`UserFriendrequestController.fuzzySearchUsers() => SQLI Attempt Detected: ${req.params.query}`));
        return res.status(400).send({ message: "Bad query given." });
    }

    let conn: PoolConnection | undefined;

    try {
        conn = await pool.getConnection();
        if (!conn) throw console.error(colors.red(`UserFriendrequestController.fuzzySearchUsers() => conn is undefined!`));
        /* const queryFuzzy: string = `SELECT * FROM users WHERE (username LIKE '%?%') OR (email LIKE '%?%')`; */
        const queryFuzzy: string = `SELECT id, username
                                   FROM users WHERE (username LIKE '%${req.params.query}%') OR (email LIKE '%${req.params.query}%')`;
        const queryFuzzyResult = await conn.query(queryFuzzy, [req.params.query, req.params.query]);
        // TODO create FuzzyUser as interface type
        const fuzzyResult: Array<{ id: number; username: string }> = [...queryFuzzyResult];

        return res.send(fuzzyResult);

    } catch (err: unknown) {
        res.status(500).send({ message: "Could not find users." });
        throw console.error(colors.red(`UserFriendrequestController.fuzzySearchUsers() => ${err}`));
    } finally {
        if (conn) conn.release();
    }
};

const getSingleFriendrequest = async (
    req: Request<{ fromUserId: string; toUserId: string }>,
    res: Response<Pending | ResponseBody>
) => {
    /**
     * Get a single friendrequest by fromUserid & toUserid
     * 
     * @param    {number}  fromUserId
     * @param    {number}  toUserId
     * 
     * @returns  {Pending} friendrequest
     */
    const fromUserid: number = Number(req.params.fromUserId);
    const toUserid: number = Number(req.params.toUserId);
    if (!fromUserid || !toUserid) return res.status(400).send({ message: `Bad user ids.` });

    let conn: PoolConnection | undefined;

    try {
        conn = await pool.getConnection();
        if (!conn) throw console.error(colors.red(`UserFriendrequestController.getSingleFriendrequest() => conn is undefined!`));

        const queryGetFriendreq: string = `SELECT 
                                               id AS pendingId, 
                                               from_user as fromUser, 
                                               to_user as toUser 
                                           FROM 
                                               friendrequests 
                                           WHERE 
                                               from_user=? 
                                           AND 
                                               to_user=?`;

        const getFriendreqResult = await conn.query(queryGetFriendreq, [fromUserid, toUserid]);
        const friendreqs: Array<Pending> = [...getFriendreqResult];

        if (!friendreqs.length) {
            console.error(colors.red(
	        `UserFriendrequestController.getSingleFriendrequest() => no pending friendrequest found!`
            )); 
            return res.status(400).send({ message: `No pending friendrequest found.` });
        }

        const friendreq: Pending = friendreqs[0];
        return res.send(friendreq);
        
    } catch (err: unknown) {
        res.status(500).send({ message: "Could not find the pending friendrequest." });
        throw console.error(colors.red(`UserFriendrequestController.getSingleFriendrequest() => ${err}`));
    } finally {
        if (conn) conn.release();
    }
};

const sendFriendrequest = async (
    req: Request<{}, {}, { fromUser: number; toUser: number }>,
    res: Response<{ friendrequest: Pending } | ResponseBody, AuthLocals>
) => {
    /**
     * Send a friendrequest
     * 
     * @body  {number}  from_user
     * @body  {string}  to_user
     * 
     * @returns  {{ message: Success; friendrequest: Pending }}
     */

    /* 0. Step:
     * Check if toUser fromUser & token are valid
     */
    const toUser: number = Number(req.body.toUser);
    if (!toUser) return res.status(401).send({ message: "Missing toUser field in body." });

    let conn: PoolConnection | undefined;

    try {
        conn = await pool.getConnection();
        if (!conn) throw console.error(colors.red(`UserFriendrequestController.sendFriendrequest() => conn is undefined!`));

        /* 1. Step
         * Check if the user is trying to send a friendrequest to himself/herself
         */
        if (toUser === res.locals.userId) {
            console.error(`UserFriendrequestController.sendFriendrequest() => you can't send yourself a friend request.`);
            return res.status(400).send({ message: "You can't send yourself a friend request." });
        }

        /* 2. Step
         * Check if he/she already has send a friendrequest to this user
         */
        const queryCheckPendingFriendrequest: string = `SELECT * FROM friendrequests
                                                       WHERE (from_user = ? AND to_user = ?)
                                                       OR (from_user = ? AND to_user = ?)`;
        const queryCheckPendingFriendrequestResult = await conn.query(
            queryCheckPendingFriendrequest,
            [res.locals.userId, toUser, toUser, res.locals.userId]
        );
        const pendingFriendrequest: Friendrequest | undefined = queryCheckPendingFriendrequestResult[0];
        if (pendingFriendrequest) return res.status(400).send({ message: `There is already a pending friend request with ${toUser}` });

        /* 3. Step
         * check if they already friends
         */
        const queryCheckIsAlreadyFriends: string = `SELECT * FROM friends
                                                   WHERE (user_id = ? AND friend_with = ?)`;
        const queryCheckIsAlreadyFriendsResult = await conn.query(queryCheckIsAlreadyFriends, [res.locals.userId, toUser]);
        const isAlreadyFriends: ServerFriend | undefined = queryCheckIsAlreadyFriendsResult[0];
        if (isAlreadyFriends) return res.status(400).send({ message: `You are already friends with the user ${toUser}` });

        /* 4. Step
         * create friendrequest
         */
        const queryCreateFriendRequest: string = `INSERT INTO friendrequests(from_user, to_user)
                                                 VALUES(?, ?)`;
        const queryCreateFriendRequestResult: OkPacket = await conn.query(queryCreateFriendRequest, [res.locals.userId, toUser]);

        /* 5. Step
         * Get the newly created pending friendrequest
         * & return it to the client
         */
        const queryGetNewPending: string = `SELECT
                                                id as pendingId,
                                                from_user as fromUser,
                                                to_user as toUser
                                            FROM
                                                friendrequests
                                            WHERE
                                                (from_user = ?)
                                            AND
                                                (to_user = ?)
                                            AND
                                                (accepted = 0)`;
        const queryGetNewPendingResult = await conn.query(queryGetNewPending, [res.locals.userId, toUser]);
        const newPending: Pending | undefined = queryGetNewPendingResult[0];
        if (!newPending) {
            console.error(colors.red(`UserFriendrequestController.sendFriendrequest() => could not retrieve the new pending friendrequest from the db`));
            return res.status(500).send({ message: "Sent the friendrequest, but couldn't retrieve it from the DB" });
        }

        return res.send({ friendrequest: newPending, });

    } catch (err: unknown) {
        res.status(500).send({ message: "Could not send a friendrequest." });
        throw console.error(colors.red(`UserFriendrequestController.sendFriendrequest() => ${err}`));
    } finally {
        if (conn) conn.release();
    }
};

const acceptFriendrequest = async (
    req: Request<{ friendreqId: string }>,
    res: Response<ResponseBody & { frienditem?: ClientFriend }, AuthLocals>
) => {
    /**
     * Accept friendrequest given by the friendrequest id
     * 
     * @params   {number}  friendreqId
     * 
     * @returns  {{ message: Success; frienditem: Friend }} 
     */
    const friendreqId: number = Number(req.params.friendreqId);
    if (!friendreqId) return res.status(400).send({ message: "Bad friendreqId." });

    let conn: PoolConnection | undefined;

    try {
        conn = await pool.getConnection();
        if (!conn) throw console.error(colors.red(`UserFriendrequestController.acceptFriendrequest() => conn is undefined!`));

        /* Get ID of sender and receiver of the friendrequest */
        const queryGetIds: string = `SELECT * FROM friendrequests WHERE id=?`;
        const queryGetIdsResult = await conn.query(queryGetIds, [friendreqId]);
        const friendRequest: Friendrequest | undefined = queryGetIdsResult[0];
        if (!friendRequest) {
            console.error(`UserFriendrequestController.acceptFriendrequest() => no ids found with the friendreqid ${friendreqId}`);
            return res.status(400).send({ message: `No Ids found with the friendreqId ${friendreqId}` });
        }

        /* Create Friend entries */
        const queryCreateFriend: string = `INSERT INTO friends(user_id, friend_with)
                                          VALUES
                                              (${friendRequest.from_user}, ${friendRequest.to_user}),
                                              (${friendRequest.to_user}, ${friendRequest.from_user})`;
        const queryCreateFriendResult: OkPacket = await conn.query(queryCreateFriend);
        /* Couldn't create friend */
        if (!queryCreateFriendResult) {
            console.error(colors.red(
	        `UserFriendrequestController.acceptFriendrequest() => Couldn't create a friend with fromUser ${friendRequest.from_user} and toUser ${friendRequest.to_user}`
            ));
            return res.status(500).send({ message: `Couldn't add ${friendRequest.to_user} to ${friendRequest.from_user}'s friends` });
        }

        /* Remove the now obsolete friendrequest */
        const queryRemove: string = `DELETE FROM friendrequests WHERE id=?`;
        const queryRemoveResult: OkPacket = await conn.query(queryRemove, [friendreqId]);
        /* Get the Friend(-item) to send it back to the client */
        const queryGetFriendItem: string = `SELECT 
                                                f.friend_with AS friendId,
                                                u.username AS friendName,
                                                u.status AS status,
                                                u.register_date AS registerDate,
                                                f.blocked AS blocked 
                                            FROM 
                                                users AS u
                                            INNER JOIN 
                                                friends AS f
                                            ON 
                                                u.id=f.friend_with
                                            WHERE 
                                                (f.user_id = ?)
                                            AND 
                                                (f.friend_with = ?)`;

        const queryGetFriendItemResult = await conn.query(queryGetFriendItem, [friendRequest.to_user, friendRequest.from_user]);
        const newFriend: ClientFriend | undefined = queryGetFriendItemResult[0];
        if (!newFriend) {
            console.error(colors.red(
	        `UserFriendrequestController.acceptFriendrequest() => accepted friendrequest, but couldn't retrieve a frienditem from the db`
            ));
            return res.status(500).send({ message: "Accepted friendrequest, but couldn't retrieve a frienditem from the db" });
        }
        conn.query(queryRemove, [friendreqId])
        return res.send({ message: "You now have a new friend", frienditem: newFriend });

    } catch (err: unknown) {
        res.status(500).send({ message: "Could not create a friend entry" });
        throw console.error(colors.red(`UserFriendrequestController.acceptFriendrequest() => ${err}`));
    } finally {
        if (conn) conn.release();
    }
};

const declineFriendrequest = async (
    req: Request<{ friendreqId: string }>,
    res: Response<ResponseBody>
) => {
    /**
     * Decline/Delete a still a open friendrequest
     * 
     * @params  {number}  friendreqId 
     * @params  {string}  token
     * 
     * @returns  {Success|Error} 
     */
    const friendreqId: number = Number(req.params.friendreqId);
    if (!friendreqId) return res.status(400).send({ message: "Bad friendreqId." });

    let conn: PoolConnection | undefined;

    try {
        conn = await pool.getConnection();
        if (!conn) throw console.error(colors.red(`UserFriendrequestController.declineFriendrequest() => conn is undefined!`));

        const query: string = `DELETE FROM friendrequests WHERE id=?`;
        conn.query(query, [friendreqId]);
        return res.status(200).send({ message: "Declined friend request" });

    } catch(err: unknown) {
        res.status(500).send({ message: "Couldn't decline friend request." });
        throw console.error(colors.red(`UserFriendrequestController.declineFriendrequest() => ${err}`));
    } finally {
        if (conn) return conn.release();
    }
};

const UserFriendrequestController = {
    fuzzySearchUsers,
    getSingleFriendrequest,
    sendFriendrequest,
    acceptFriendrequest,
    declineFriendrequest,
};

export default UserFriendrequestController;
