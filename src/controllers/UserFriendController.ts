import { Request, Response } from "express";
import { SqlError, PoolConnection } from "mariadb";
import colors from "colors";

import pool from "../db/db";
import type { OkPacket } from "../ts/types/mariadb_types";
import type { Friend } from "../ts/types/db_types";
import type { ResponseBody, AuthLocals } from "../ts/types/express_types";

const removeFriend = async (
    req: Request<{}, {}, { friendId: number }>,
    res: Response<ResponseBody, AuthLocals>
) => {
    /**
     * Remove a friend
     * 
     * @body  {number}  friendId
     * 
     * @returns  {Success|Error}
     */
    const friendId: number = Number(req.body.friendId);
    if (!friendId) return res.status(400).send({ message: "Bad friendId." });

    let conn: PoolConnection | undefined;

    try {
        conn = await pool.getConnection();
        if (!conn) throw console.error(colors.red(`UserFriendController.removeFriend() => conn is undefined!`));

        const query: string = `DELETE FROM friends
                              WHERE (user_id=? AND friend_with=?)
                              OR (user_id=? AND friend_with=?)`;

        conn.query(query, [res.locals.userId, friendId, friendId, res.locals.userId]);
        return res.status(200).send({ message: "Friend has been removed." });

    } catch(err: unknown) {
        res.status(500).send({ message: `Internal Server Error: Couldn't remove friend with the id ${friendId}`});
        throw console.error(colors.red(`UserFriendController.removeFriend() => ${err}`));
    } finally {
        if (conn) conn.release();
    }
};

const blockFriend = async (
    req: Request<{}, {}, { friendId: number }>,
    res: Response<ResponseBody, AuthLocals>
) => {
    /**
     * Block a User
     * 
     * @body  {number}  friendId
     * 
     * @returns  {Success|Error}
     */
    const friendId: number = Number(req.body.friendId);
    if (!friendId) return res.status(400).send({ message: `Bad friendId.` });

    let conn: PoolConnection | undefined;

    try {
        conn = await pool.getConnection();
        if (!conn) throw console.error(colors.red(`UserFriendController.blockFriend() => conn is undefined!`));

        /* TODO Check if you are/aren't friends */

        /* Update Blocked to true */
        const queryBlock: string = `UPDATE friends
                                   SET blocked=1
                                   WHERE (user_id=? AND friend_with=?)`;

        conn.query(queryBlock, [res.locals.userId, friendId]);
        return res.status(200).send({ message: `User have been blocked.` });
       
    } catch(err: unknown) {
        res.status(500).send({ message: `Couldn't block this user` });
        throw console.error(colors.red(`UserFriendController.blockFriend() => ${err}`));
    } finally {
        if (conn) return conn.release();
    }
};

const unblockFriend = async (
    req: Request<{}, {}, { friendId: number }>,
    res: Response<ResponseBody, AuthLocals>
) => {
    /**
     * Unblock a blocked user
     * 
     * @body  {number}  friendId
     * 
     * @returns  {Success|Error}
     */
    const friendId: number = Number(req.body.friendId);
    if (!friendId) return res.status(400).send({ message: "Bad friendId." });

    let conn: PoolConnection | undefined;

    try {
        conn = await pool.getConnection();
        if (!conn) throw console.error(colors.red(`UserFriendController.unblockFriend() => conn is undefined!`));

        /* Update Blocked to false */
        const queryUnblock: string = `UPDATE friends
                                     SET blocked=0
                                     WHERE (user_id=? AND friend_with=?)`;

        conn.query(queryUnblock, [res.locals.userId, friendId])
        return res.status(200).send({ message: `User have been unblocked.` });

    } catch(err: unknown) {
        res.status(500).send({ message: `Couldn't unblock this user` });
        throw console.error(colors.red(`UserFriendController.unblockFriend() => ${err}`));
    } finally {
        if (conn) return conn.release();
    }
};

const isFriendWith = async (
    req: Request<{ firstUserId: string; secondUserId: string }>,
    res: Response<boolean | ResponseBody, AuthLocals>
) => {
    /**
     * Check if a user is friend with a another user
     * this route is necessary for fast checking if the send message input
     * of the client should be disabled or not
     * 
     * @param  {number}  firstUserId
     * @param  {number}  secondUserId
     *
     * @returns  {boolean}
     */
    const firstUserId: number = Number(req.params.firstUserId);
    const secondUserId: number = Number(req.params.secondUserId);
    if (!firstUserId || !secondUserId) return res.status(400).send({ message: `Bad ids given.` });

    let conn: PoolConnection | undefined;

    try {
        conn = await pool.getConnection();
        if (!conn) throw console.error(colors.red(`UserFriendController.isFriendWith() => conn is undefined!`));

        /* check if they're friends */
        const query: string = `SELECT * FROM friends 
                              WHERE user_id=? 
                              AND friend_with=?`;

        const queryResult = await conn.query(query, [firstUserId, secondUserId]);
        const friend: Friend | undefined = queryResult[0];

        if (!friend) return res.send(true);
        return res.send(false);

    } catch (err: unknown) {
        res.status(500).send({ message: "A Server Error occurred." });
        throw console.error(colors.red(`UserFriendController.isFriendWith() => ${err}`));
    } finally {
        if (conn) return conn.release();
    }
};

const UserFriendController = {
    removeFriend,
    blockFriend,
    unblockFriend,
    isFriendWith,
};

export default UserFriendController;
