import jwt, { JwtPayload } from "jsonwebtoken";

const getIdFromToken: (token: string) => number | null = (token) => {
  const jwtPayload: string | JwtPayload | null = jwt.decode(token);
  /* unvalid jwt */
  if (!jwtPayload || typeof jwtPayload === "string") {
    return null;
  }
  const id: number = jwtPayload.id;
  return id;
};

export default getIdFromToken;
