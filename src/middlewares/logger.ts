import path from "node:path";
import fs from "node:fs";
import { Application } from "express";
import morgan from "morgan";
import colors from "colors";

const app = (app: Application) => {
    const accessLogStream = fs.createWriteStream(
        path.join(__dirname, "access.log"),
        { flags: "a" }
    );

    const format: string = colors.gray(
        ':remote-addr - :remote-user [:date[clf]] ":method :url HTTP/:http-version" :status :res[content-length] ":referrer" ":user-agent"'
    );

    if (process.env.NODE_ENV ==="dev"){
        app.use(morgan(format));
    } else {
        app.use(morgan("combined", { stream: accessLogStream }));
    }
};

export default { app };
