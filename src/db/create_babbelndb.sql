-- To create the db use the following Command
-- mysql -u root < Path/to/the/file/create_doomdb.sql

CREATE DATABASE IF NOT EXISTS babbelndb;
USE babbelndb;

-- create hybadb user
CREATE OR REPLACE USER 'babbeln'@'localhost' IDENTIFIED BY 'babbelnpassword';
CREATE OR REPLACE USER 'babbeln'@'172.17.0.1' IDENTIFIED BY 'babbelnpassword'; -- docker
GRANT ALL PRIVILEGES ON babbelndb.* TO 'babbeln'@'localhost';
GRANT ALL PRIVILEGES ON babbelndb.* TO 'babbeln'@'172.17.0.1'; -- docker
FLUSH PRIVILEGES;

CREATE TABLE IF NOT EXISTS users(
       id INT AUTO_INCREMENT,
       username VARCHAR(15),
       email VARCHAR(100),
       password VARCHAR(255),
       is_active TINYINT(1) DEFAULT 0,
       --       socketid VARCHAR(255) DEFAULT 'offline',
       status VARCHAR(4) DEFAULT 'on',
       register_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
       PRIMARY KEY(id)
);

CREATE TABLE IF NOT EXISTS socketids(
       socketid VARCHAR(255),
       user_id INT UNSIGNED NOT NULL,
       PRIMARY KEY(socketid)
);

CREATE TABLE IF NOT EXISTS messages(
       id INT AUTO_INCREMENT,
       owner INT UNSIGNED NOT NULL,
       from_user INT UNSIGNED NOT NULL,
       to_user INT UNSIGNED NOT NULL,
       content VARCHAR(2000),
       -- send_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
       send_at BIGINT UNSIGNED NOT NULL,
       PRIMARY KEY(id)
);

CREATE TABLE IF NOT EXISTS friendrequests(
       id INT AUTO_INCREMENT,
       from_user INT UNSIGNED NOT NULL,
       to_user INT UNSIGNED NOT NULL,
       -- this one should propably be removed, it stays here for error checking. no request should have accepted = 1
       accepted TINYINT(1) DEFAULT 0, 
       PRIMARY KEY(id, from_user, to_user)
       -- if declined delete the request
);

CREATE TABLE IF NOT EXISTS friends(
       id INT AUTO_INCREMENT,
       user_id INT UNSIGNED NOT NULL,
       friend_with INT UNSIGNED NOT NULL,
       blocked TINYINT(1) DEFAULT 0,
       PRIMARY KEY(id, user_id, friend_with)
);

CREATE TABLE IF NOT EXISTS posts(
       id INT AUTO_INCREMENT,
       written_by INT UNSIGNED NOT NULL,
       content VARCHAR(200),
       written_at BIGINT UNSIGNED NOT NULL,
       PRIMARY KEY(id, written_by)
);

CREATE TABLE IF NOT EXISTS post_likes(
       user_id INT UNSIGNED NOT NULL,
       post_id INT UNSIGNED NOT NULL,
       PRIMARY KEY(user_id, post_id)
);

CREATE TABLE IF NOT EXISTS post_images(
       post_id INT UNSIGNED NOT NULL,
       filename VARCHAR(255) NOT NULL,
       PRIMARY KEY(post_id, filename)
);

CREATE TABLE IF NOT EXISTS post_comments(
       id INT AUTO_INCREMENT, 
       post_id INT UNSIGNED NOT NULL,
       user_id INT UNSIGNED NOT NULL,
       content VARCHAR(255),
       written_at BIGINT UNSIGNED NOT NULL,
       PRIMARY KEY(id)
);


