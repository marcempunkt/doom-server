import process from "node:process";
import mariadb, { Pool, PoolConnection } from "mariadb";
import Config from "../lib/config";

const config: Config = new Config();
config.env();

const pool: Pool = mariadb.createPool({
    /* host: config.db.host,
     * user: config.db.user,
     * password: config.db.pass,
     * database: config.db.name, */
    host: process.env.DB_HOST,
    user: process.env.DB_USER,
    password: process.env.DB_PASS,
    database: process.env.DB_NAME,
});

const connector = {
  getConnection: () => {
    return new Promise<PoolConnection>((resolve, reject) =>{
      pool.getConnection().then((connection: PoolConnection) =>{
        resolve(connection);
      }).catch((error: unknown) => {
        reject(error);
      });
    });
  }
};

export default connector;
