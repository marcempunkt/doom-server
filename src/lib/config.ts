import fs from "node:fs";
import path from "node:path";
import process from "node:process";
import colors from "colors";
import { v4 as uuidv4 } from "uuid";

interface ParsedConfig {
    db?: {
        host?: string;
        user?: string;
        pass?: string;
        name?: string;
    },
    auth?: {
        secret?: string;
    };
    server?: {
        port?: number;
    };
}

export default class Config {
    /** Typing */
    db: {
        host: string;
        user: string;
        pass: string;
        name: string;
    };
    auth: {
        secret: string;
    };
    server: {
        port: number;
    };


    constructor() {
        /**
         * Read the babbeln-server config file
         */

        const configPaths: Array<string> = [
            "./.babbeln.config.json",
        ];

        let parsedConfig: ParsedConfig = {};

        for (const configPath of configPaths) {
            try {
                const content: string = fs.readFileSync(configPath, "utf-8");
                parsedConfig = JSON.parse(content) as ParsedConfig;
                break;
            } catch(e: any) {
                switch (e?.name) {
                    case "SyntaxError": {
                        throw Error(colors.red(
                            `SyntaxError while parsing ${configPath}: ${e.message}`  
                        ))
                    }
                    default: {
                        continue;
                    }
                }
            }
        }

        /** Create sensible defaults */
        const dbHost: string = parsedConfig?.db?.host || "0.0.0.0";
        const dbUser: string = parsedConfig?.db?.user || "user";
        const dbPass: string = parsedConfig?.db?.pass || "password";
        const dbName: string = parsedConfig?.db?.name || "database";
        const secret: string = parsedConfig?.auth?.secret || uuidv4();
        const port:   number = parsedConfig?.server?.port || 3001;

        this.db = {
            host: dbHost,
            user: dbUser,
            pass: dbPass,
            name: dbName,
        };

        this.auth = {
            secret: secret,
        };

        this.server = {
            port: port,
        };

        /* throw Error("Failed to initialize Config Object: Missing server.port field"); */
    }

    env() {
        /**
         * TODO still used inside db.ts 
         */
        process.env.DB_HOST = this.db.host;
        process.env.DB_USER = this.db.user;
        process.env.DB_PASS = this.db.pass;
        process.env.DB_NAME = this.db.name;
        process.env.SECRET_TOKEN = this.auth.secret;
        process.env.PORT = String(this.server.port);
    }
}
