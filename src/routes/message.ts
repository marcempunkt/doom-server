import { Router } from "express";
import authentication from "../middlewares/authentication";
import MessageController from "../controllers/MessageController";

const router: Router = Router();

router.get("/",
           authentication,
           MessageController.getMessages);

router.post("/send",
            authentication,
            MessageController.sendMessage);

router.delete("/delete/globally/:msgId",
              authentication,
              MessageController.deleteGloballyMessage);

router.delete("/delete/privately/:msgId",
              authentication,
              MessageController.deletePrivatelyMessage);

router.patch("/delete/chat",
             authentication,
             MessageController.deleteChat);

export default router;
