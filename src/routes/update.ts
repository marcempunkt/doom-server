import { Router, Request, Response } from "express";
import path from "path";

const router: Router = Router();

router.post("/check", (req: Request, res: Response) => {
  res.write(JSON.stringify({
    "version": "0.0.1",
    "sha1": "203448645d8a32b9a08ca9a0eb88006f874d0c78",
  }).replace(/[\/]/g, '\\/'));
  res.end();
});

router.get("/latest", (req: Request, res: Response) => {
  const latestAsar: string = path.join("");
  res.sendFile(latestAsar);
});

export default router;
