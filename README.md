<img src="https://gitlab.com/marcempunkt/babbeln-client/-/raw/master/readme/logo.png" align="right" width="100" height="100" />

# Babbeln-server 
> Server for the free & privacy oriented messenger with unlimited voice and video calls called `babbeln-client`.  
> Licensed under `GNU GPLv3`

<div align="center">[Introduction](#Introduction) • [Install](#install) • [Contribute](#contribute)</div>

## Project Goal(s)

The goal is to make an attractive alternative to 'zoom' or 'discord', but as an easy fork-able project so that you can host your very own server.  
The `babbeln-server` is used together with the `babbeln-client`. It is the back-end service that handles sending/receiving
messages, initiating the p2p connection for two users (group chats will be added in the future) to start voice/video calls.

## Project structure

```
babbeln-sever
|  README.md
|--src
|  index.ts          Entry Point
|  |--routes         Routes & some API files (needs to be changed) 
|  |--api            API
|  |--sql            SQL Scripts
|  |--ts             TS types
|--docker            Scripts for docker
|  |--data           Volume for babbelndb 
|--drawio            Schematics/Visualisations
|--files             Content that was uploaded by the user
```

## To get the server running follow these steps:

Clone the repo & install all dependencies:

```
git clone https://gitlab.com/marcempunkt/babbeln-server
npm i
```

Next is to create a `.env` file inside the root of the `babbeln-server` project:

```
DB_HOST=127.17.0.2
DB_USER=babbeln
DB_PASSWORD=babbelnpassword
DB_DATABASE=babbelndb
SECRET_TOKEN=random_secret_token_for_jsw
```

Then you can create the db and start the server:

```
npm run db:create
npm run db:start
npm run start
```

## NPM scripts 

`npm run <script-name>`

| Script name                    | Description                                                     |
| ------------------------------ | --------------------------------------------------------------- |
| start                          | Start the server                                                |
| db:create                      | Create the database inside a docker container                   |
| db:start                       | Start the db docker container                                   |
| db:stop                        | Stop the db docker container                                    |
| db:remove                      | Remove the db docker container                                  |
| db:recreate                    | Shortcut for db:stop && db:remove && db:create && db:start      |
| db:exec                        | Go into the mysql repl of the db                                |
| db:bash                        | Go into te shell of the db container                            |

## Routes

Those are currently not up-to-date!!!!!!

### `/routes/api/user.ts`

| Route                                              | HTTP Method | Protected? | Description                                                     |
| -------------------------------------------------- | ----------- | ---------- | --------------------------------------------------------------- |
| /api/user/validate-token                           | POST        | ❌          | validate the token of a logged in user                          |
| /api/user/register                                 | POST        | ❌          | register                                                        |
| /api/user/login                                    | POST        | ❌          | login                                                           |
| /api/user/delete-user                              | DELETE      | ☑          | delete your account                                             |

---

### `routes/api/user/profile.ts`

| Route                                              | HTTP Method | Protected? | Description                                                     |
| -------------------------------------------------- | ----------- | ---------- | --------------------------------------------------------------- |
| /api/user/profile/get-pic/:userId                  | GET         | ❌          | get the profile pic of a user                                   |
| /api/user/profile/get-status/:userId               | GET         | ❌          | get the status of a user                                        |
| /api/user/profile/get-registerdate/:userId         | GET         | ❌          | get the registered date of a user                               |

---

### `/routes/api/user/profile/change.ts`

| Route                                              | HTTP Method | Protected? | Description                                                     |
| -------------------------------------------------- | ----------- | ---------- | --------------------------------------------------------------- |
| /api/user/profile/change/username                  | PATCH       | ☑          | change username                                                 |
| /api/user/profile/change/email                     | PATCH       | ☑          | change email                                                    |
| /api/user/profile/change/password                  | PATCH       | ☑          | change password                                                 |

---

### `/routes/api/user/findby.ts`

| Route                                              | HTTP Method | Protected? | Description                                                     |
| -------------------------------------------------- | ----------- | ---------- | --------------------------------------------------------------- |
| OBSOLTE? /api/user/findby/name/:username                    | GET         | ❌          | find a user by its username                                     |
| OBSOLTE? /api/user/findby/id/:id                            | GET         | ❌          | find a user by its id                                           |
| OBSOLTE? /api/user/findby/token                             | POST        | ❌          | find a user by its token                                        |

---

### `/routes/api/user/friendrequest.ts`

| Route                                              | HTTP Method | Protected? | Description                                                     |
| -------------------------------------------------- | ----------- | ---------- | --------------------------------------------------------------- |
| /api/user/friendrequest/add                        | GET         | ☑          | send a friend request to a user                                 |
| /api/user/friendrequest/accept/:friendreqId/:token | PATCH       | ☑          | accept a friend request                                         |
| /api/user/friendrequest/decline/:friendreqId/:token| PATCH       | ☑          | decline a friend request                                        |
| /api/user/friendrequest/fuzzy-search/:query        | GET         | ❌          | fuzzy search for users                                          |

---

### `/routes/api/user/friend.ts`

| Route                                              | HTTP Method | Protected? | Description                                                     |
| -------------------------------------------------- | ----------- | ---------- | --------------------------------------------------------------- |
| /api/user/friend/remove                            | PATCH       | ❌          | remove a friend                                                 |
| /api/user/friend/block                             | PATCH       | ❌          | block a friend                                                  |
| /api/user/friend/unblock                           | PATCH       | ❌          | unblock a friend                                                |
| OBSOLTE? /api/user/friend/is-friends-with/:userId/:friendId | GET         | ❌          | check if the logged in user is friend with "John Doe"           |

---

### `/routes/api/user/friend/get.ts`

| Route                                              | HTTP Method | Protected? | Description                                                     |
| -------------------------------------------------- | ----------- | ---------- | --------------------------------------------------------------- |
| /api/user/friend/get/pending/:userId               | GET         | ❌          | get all pending friend requests of a user                       |
| /api/user/friend/get/blocked/:userId               | GET         | ❌          | get all blocked friends of a user                               |
| /api/user/friend/get/friends/:userId               | GET         | ❌          | get all friends of a user                                       |

---

### `/routes/api/message.ts`

| Route                                              | HTTP Method | Protected? | Description                                                     |
| -------------------------------------------------- | ----------- | ---------- | --------------------------------------------------------------- |
| /api/message/from-user/:token                      | GET         | ❌          |  GET message of a logged in user                                |
| /api/message/delete/globally/:msgId/:token         | PATCH       | ☑          |  delete msg for all users                                       |
| /api/message/delete/privately/:msgId/:token        | DELETE      | ☑          |  delete msg only for one privately                              |
| /api/message/delete/chat                           | PATCH       | ☑          |  delete a whole chat between two users (privately)              |

# Socket Events

![Socketio Schematic](https://gitlab.com/marcempunkt/babbeln-server/-/raw/master/drawio/socketio_schematic.jpg "Socketio schematic")

# MariaDB Schematic

![MariaDB Schematic](https://gitlab.com/marcempunkt/babbeln-server/-/raw/master/drawio/mariadb_schematic.jpg "MariaDB schematic")

## Contribute

I'm a japanese & korean studies student. I'm not a cs student so therefore any criticism, pull requests or advices are highly appreciated!!!

Don't hestitate to tell me that my code sucks and that I should stick to teaching languages:   
+ <a href="mailto:marc.maeurer@pm.me">Send me an Email</a>

