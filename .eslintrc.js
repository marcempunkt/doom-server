module.exports = {
    env: {
        node: true
    },
    extends: [
        "eslint:recommended",
        "plugin:@typescript-eslint/recommended",
    ],
    parser: "@typescript-eslint/parser",
    parserOptions: {
        ecmaVersion: "latest",
    },
    plugins: [
        "@typescript-eslint"
    ],
    rules: {
        /* disabled */
        "no-mixed-spaces-and-tabs": 0,
        "no-useless-escape": 0,
        "@typescript-eslint/no-inferrable-types": 0,
        "@typescript-eslint/no-non-null-assertion": 0,
        "@typescript-eslint/no-explicit-any": 0,
        /* warnings */
        "no-async-promise-executor": "warn",
        "no-unsafe-finally": "warn",
        "no-case-declarations": "warn",
        "no-empty": "warn",
        "prefer-const": "warn",
        "@typescript-eslint/no-extra-semi": "warn",
        "@typescript-eslint/ban-ts-comment": "warn",
        "@typescript-eslint/ban-types": "warn",
        "@typescript-eslint/no-empty-function": "warn",
        /* custom */
        "no-unused-vars": "off",
        "no-irregular-whitespace": ["error", { "skipRegExps": true }],
        "@typescript-eslint/no-unused-vars": ["warn", { "argsIgnorePattern": "^_" }],
    },
};
