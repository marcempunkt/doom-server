#!/bin/bash
docker run --name babbelndb \
       -e MYSQL_ROOT_PASSWORD=mypass \
       -p 3306:3306 \
       -v $PWD/docker/db/data:/var/lib/mysql \
       -d mariadb:10.6

echo "Wait one minute for the mysql socket to properly start..."
sleep 60s

echo "Creating Database..."
docker cp src/db/create_babbelndb.sql babbelndb:/home/create_babbelndb.sql
docker exec -it babbelndb /bin/sh -c 'mysql -u root --password=mypass < /home/create_babbelndb.sql'

echo "Creating Database was successfull!"
